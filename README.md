# Actix Web Project

This repository contains the source code for a web application built with Actix, a pragmatic and fast web framework for Rust. The application has been containerized using Docker for easy deployment and scalability.

## Overview

The Actix web service provides several routes that will respond with different html responses. This project is set up to run within a Docker container, ensuring a consistent and isolated environment across different stages of development, testing, and production.

## Prerequisites

Before you begin, ensure you have the following installed on your system:
- Docker
- Rust (if you plan to develop or build locally)

## Building the Docker Image

To build the Docker image for the Actix web service, navigate to the root directory of this project and run:

```bash
docker build -t actix-web-app .
```

This command builds a Docker image named `actix-web-app` based on the instructions in the `Dockerfile`. 

## Running the Container

After building the image, you can run the Actix web service in a Docker container using:

```bash
docker run -d -p 8080:8080 actix-web-app
```

This command runs the `actix-web-app` container in detached mode (`-d`) and maps port 8080 of the container to port 8080 on the host, allowing you to access the web service at `http://localhost:8080`.

The following screenshots show that the web application is indeed running in a container, and is also accessible from the host laptop's browser:

### Running Container from Built Image
![Running Container from Image actix-web-app](docker-running.png)

### Root Route for the Website
![Root Route for the Website](docker-actix-route1.png)

### Manually Defined Route for the Website
![Manually Defined Route for the Website](docker-actix-route2.png)

## Customization

- To customize the application, you can modify the source code in the `src` directory. After making changes, rebuild the Docker image to apply them.
- For specific configurations related to Actix or Docker, refer to the `main.rs` file or the `Dockerfile`, respectively.